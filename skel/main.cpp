#include "main.h"
#include <stdio.h>
int main() {
	uint8_t buffer[2] = {16, 0};
	auto person = Person::decode_from_u8(buffer);
        uint8_t *decoded = person.encode_to_u8();
	printf("%d\n", person.get_age());
	printf("%d %d\n", decoded[0], decoded[1]);
	return 0;
}
