from pathlib import Path
from fcp.codegen import CodeGenerator
from fcp.verifier import BaseVerifier
from fcp_serialization import packed
from .codegen import (
    Header,
    Define,
    Scope,
    Include,
    Class,
    ClassMethod,
    Function,
    FunctionArg,
    Attribute,
    Visibility,
)


class Verifier(BaseVerifier):
    pass

class Generator(CodeGenerator):
    def generate_encoders(self, fcp_v2, struct):
        allocations = list(packed.allocate_message(fcp_v2, struct))
        signals = [
            self.generate_encode_signal(*allocation)
            for allocation in allocations
        ]

        total_size = allocations[-1][1] + allocations[-1][2]

        return Function(
            "encode_to_u8",
            args=[],
            ret="inline u8*",
            statements=[f"uint8_t *buffer = (uint8_t *) malloc({total_size})"] + signals  + ["return buffer"],
        )

    def generate_encode_signal(self, signal, position, length):
        return f"memcpy(buffer+{position}, &{signal}, {length});"

    def generate_decode_signal(self, signal, position, length):
        return f"memcpy(&obj.{signal}, buffer+{position}, {length})"

    def generate_decoders(self, fcp_v2, struct):
        allocations = packed.allocate_message(fcp_v2, struct)
        
        signals = [
            self.generate_decode_signal(*allocation)
            for allocation in allocations
        ]

        return Function(
            "decode_from_u8",
            args=[FunctionArg("u8*", "buffer")],
            ret="inline static " + struct.name,
            statements=[struct.name + " obj = {}"] + signals + ["return obj"],
        )

    def generate_struct(self, fcp_v2, struct):
        return Class(
            struct.name,
            methods=[
                ClassMethod(
                    Function(
                        name="get_" + signal.name,
                        args=[],
                        ret=signal.type,
                        statements=[f"return {signal.name};"],
                    ),
                    Visibility.Public,
                )
                for signal in struct.signals
            ]
            + [
                ClassMethod(
                    Function(
                        name="set_" + signal.name,
                        args=[
                            FunctionArg(signal.type, signal.name)
                            for signal in struct.signals
                        ],
                        ret="void",
                        statements=[f"{signal.name} = {signal.name};"],
                    ),
                    Visibility.Public,
                )
                for signal in struct.signals
            ]
            + [
                ClassMethod(self.generate_decoders(fcp_v2, struct), Visibility.Public),
                ClassMethod(self.generate_encoders(fcp_v2, struct), Visibility.Public),
            ],
            attributes=[
                Attribute(signal.type, signal.name) for signal in struct.signals
            ],
        )


    def generate(self, fcp, output_path, templates={}, skels={}):
        header = Header(
            [
                Include("stdint.h", Scope.Global),
                Include("string.h", Scope.Global),
                Include("cstdlib", Scope.Global),
                Define("u8", "uint8_t"),
                Define("u16", "uint16_t"),
                Define("u32", "uint32_t"),
                Define("u64", "uint64_t"),
                Define("i8", "int8_t"),
                Define("i16", "int16_t"),
                Define("i32", "int32_t"),
                Define("i64", "int64_t"),
            ]
            + [self.generate_struct(fcp, struct) for struct in fcp.structs]
        )

        return {output_path / "main.h": header.generate()} | {
            output_path / key: value for key, value in skels.items()
        }
