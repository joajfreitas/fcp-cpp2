from enum import Enum


class Attribute:
    def __init__(self, type, name):
        self.type = type
        self.name = name

    def generate(self):
        return self.type + " " + self.name + ";"


class FunctionArg:
    def __init__(self, type, name):
        self.type = type
        self.name = name

    def generate(self):
        return self.type + " " + self.name


class Function:
    def __init__(self, name, args, ret, statements):
        self.name = name
        self.args = args
        self.ret = ret
        self.statements = statements

    def generate(self):
        return (
            self.ret
            + " "
            + self.name
            + "("
            + ",".join([arg.generate() for arg in self.args])
            + ")"
            + "{"
            + ";\n".join(self.statements) + ";"
            + "}"
        )


class Visibility(Enum):
    Public = 0
    Private = 1


class ClassMethod:
    def __init__(self, function, visibility):
        self.function = function
        self.visibility = visibility

    def generate(self):
        return self.function.generate()


class Class:
    def __init__(self, name, methods, attributes):
        self.name = name
        self.methods = methods
        self.attributes = attributes

    def generate(self):
        public_methods = filter(
            lambda method: method.visibility == Visibility.Public, self.methods
        )
        private_methods = filter(
            lambda method: method.visibility == Visibility.Private, self.methods
        )

        return (
            "class "
            + self.name
            + "{\n"
            + "public:\n"
            + ";\n".join([method.generate() for method in public_methods])
            + ";"
            + "\nprivate:\n"
            + "\n".join([method.generate() for method in private_methods])
            + "\n".join([attribute.generate() for attribute in self.attributes])
            + "};"
        )


class Scope(Enum):
    Global = 0
    Relative = 0


class Include:
    def __init__(self, path, scope):
        self.path = path
        self.scope = scope

    def generate(self):
        if self.scope == Scope.Global:
            return "#include <" + self.path + ">"
        else:
            return '#include "' + self.path + '"'


class Define:
    def __init__(self, name, value):
        self.name = name
        self.value = value

    def generate(self):
        return "#define " + self.name + " " + self.value


class Header:
    def __init__(self, objs):
        self.objs = objs

    def generate(self):
        return "\n".join([obj.generate() for obj in self.objs])
